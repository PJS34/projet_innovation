// HandRecognitionProject.cpp : définit le point d'entrée pour l'application console.
//

#include "stdafx.h"

//#include "handRecognition.h"

#include<opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

Mat src, src_gray, dst;
int threshold_value = 220;
int threshold_type = 1;
int const max_value = 255;
int const max_type = 4;
int const max_BINARY_value = 255;

int angleMax = 200;
int angleMin = 20;

/**
* Permet de calculer les angles entre deux segments du contours de la forme
*/
float innerAngle(float px1, float py1, float px2, float py2, float cx1, float cy1)
{

	float dist1 = std::sqrt((px1 - cx1)*(px1 - cx1) + (py1 - cy1)*(py1 - cy1));
	float dist2 = std::sqrt((px2 - cx1)*(px2 - cx1) + (py2 - cy1)*(py2 - cy1));

	float Ax, Ay;
	float Bx, By;
	float Cx, Cy;
	Cx = cx1;
	Cy = cy1;
	if (dist1 < dist2)
	{
		Bx = px1;
		By = py1;
		Ax = px2;
		Ay = py2;


	}
	else {
		Bx = px2;
		By = py2;
		Ax = px1;
		Ay = py1;
	}


	float Q1 = Cx - Ax;
	float Q2 = Cy - Ay;
	float P1 = Bx - Ax;
	float P2 = By - Ay;


	float A = std::acos((P1*Q1 + P2 * Q2) / (std::sqrt(P1*P1 + P2 * P2) * std::sqrt(Q1*Q1 + Q2 * Q2)));

	A = A * 180 / CV_PI;

	return A;
}

/**
* Permet de calculer les creux de la main par rapport à l'enveloppe convexe
* On ne s'en sert pas dans le programme
*/
void condefects(vector<Vec4i> convexityDefectsSet, vector<cv::Point> mycontour, Mat &frame)
{
	Point2f mycenter;
	float myradius;
	float dis;
	vector<cv::Point>fingertips, deptharr;

	//std::vector<cv::Point> validPoints;
	for (int cDefIt = 0; cDefIt < convexityDefectsSet.size(); cDefIt++) {

		int startIdx = convexityDefectsSet[cDefIt].val[0]; cv::Point ptStart(mycontour[startIdx]);

		int endIdx = convexityDefectsSet[cDefIt].val[1]; cv::Point ptEnd(mycontour[endIdx]);

		int farIdx = convexityDefectsSet[cDefIt].val[2]; cv::Point ptFar(mycontour[farIdx]);

		//double angle = std::atan2(center.y - ptStart.y, center.x - ptStart.x) * 180 / CV_PI;
		double inAngle = innerAngle(ptStart.x, ptStart.y, ptEnd.x, ptEnd.y, ptFar.x, ptFar.y);
		double length = std::sqrt(std::pow(ptStart.x - ptFar.x, 2) + std::pow(ptStart.y - ptFar.y, 2));
		//double centerLength = std::sqrt(std::pow(ptStart.x - center.x, 2) + std::pow(ptStart.y - center.y*1.2, 2));
		double depth = static_cast<double>(convexityDefectsSet[cDefIt].val[3]) / 256;
		cout << abs(inAngle);
		if (/*angle > -100 && angle < 200 &&*/ std::abs(inAngle) > angleMin && std::abs(inAngle) < angleMax /*&& length > 0.15 * boundingBox.height*/)
		{
			fingertips.push_back(ptStart);
			cout << " hit";
		}
		cout << endl;
		//if (depth > 0 && depth < 120 && std::abs(inAngle) > 20 && std::abs(inAngle) < 120 && angle > -160 && angle < 160 && centerLength > lengthMin/100*boundingBox.height)
		//{
		//	line(frame, cv::Point(center.x / 1.1, center.y*1.2), ptStart,Scalar(0,255,0),1,8);
		//	circle(frame, ptStart, 20, Scalar(0, 255, 0), 1,8);
		//	fingertips.push_back(ptStart);
		//}
		if (depth > 10 && depth < 120)
		{
			circle(frame, ptFar, 3, Scalar(100, 0, 255), 2, 8);
			deptharr.push_back(ptFar);
		}


	}
	cout << "contour size : " << mycontour.size() << endl;
	/*
	/// Get the moments
	vector<Moments> mu(mycontour.size());
	for (int i = 0; i < mycontour.size(); i++)
	{
		mu[i] = moments(mycontour, false);
	}*/
	Moments mu = moments(mycontour, false);
	/*
	///  Get the mass centers:
	vector<Point2f> mc(mycontour.size());
	for (int i = 0; i < mycontour.size(); i++)
	{
		mc[i] = Point2f(mu[i].m10 / mu[i].m00, mu[i].m01 / mu[i].m00);
	}*/
	//Barycentre
	Point2f mc = Point2f(mu.m10 / mu.m00, mu.m01 / mu.m00);


	cv::circle(frame, mc, 1, cv::Scalar(200, 0, 255), 5);
	cv::Point2f centerps;
	float radips;
	if (deptharr.size() > 3)
	{
		cv::minEnclosingCircle(deptharr, centerps, radips);
		//circle(frame, centerps, radips, Scalar(0, 255, 0), 1, 8);
	}

	for (size_t i = 0; i < fingertips.size(); i++)
	{
		if (deptharr.size() > 3)
		{
			line(frame, centerps, fingertips[i], Scalar(0, 255, 0), 1, 8);
		}
		cv::circle(frame, fingertips[i], 3, cv::Scalar(100, 0, 255), 5);
		line(frame, mc, fingertips[i], Scalar(200, 0, 255), 1, 8);
	}

	if (fingertips.size() == 1)
	{
		putText(frame, "01", cv::Point(400, 50), 1, 2, CV_RGB(255, 0, 0), 2, 8);
	}
	else if (fingertips.size() == 2)
	{
		putText(frame, "02", cv::Point(400, 50), 1, 2, CV_RGB(255, 0, 0), 2, 8);
	}
	else if (fingertips.size() == 3)
	{
		putText(frame, "03", cv::Point(400, 50), 1, 2, CV_RGB(255, 0, 0), 2, 8);
	}
	else if (fingertips.size() == 4)
	{
		putText(frame, "04", cv::Point(400, 50), 1, 2, CV_RGB(255, 0, 0), 2, 8);
	}
	else if (fingertips.size() == 5)
	{
		putText(frame, "05", cv::Point(400, 50), 1, 2, CV_RGB(255, 0, 0), 2, 8);
	}
	//work will start from here
	//if (krokaam)
	//workOnDefects(frame, fingertips, deptharr);

}

/**
* Permet de calculer le centre de masse de la forme définie par la liste de vecteur en entrée (les contours de la forme)
*/
Point2f center_of_mass(vector<cv::Point> mycontour) {
	Moments mu = moments(mycontour, false);
	Point2f mc = Point2f(mu.m10 / mu.m00, mu.m01 / mu.m00);
	return mc;
}

/**
* Permet de détecter et d'afficher les extrémités de la forme
*/
void convex_point(vector<cv::Point> mycontour, Mat &frame) {
	Point2f mc = center_of_mass(mycontour);
	Point p = mycontour[0];
	vector<cv::Point> points;
	vector<cv::Point> fingers_points;
	cv::circle(frame, mc, 2, cv::Scalar(200, 0, 255), 5);
	points.push_back(mycontour[0]);
	int minx = 0;
	int miny = 0;
	for (int i = 1; i < mycontour.size(); i++) {

	}
	for (int i = 1; i < mycontour.size(); i++) {
		if (abs(mycontour[i].x - p.x) < 30 && abs(mycontour[i].y - p.y) < 30) {
			points.push_back(mycontour[i]);
			p = mycontour[i];
		}
		else {
			fingers_points.push_back(points[points.size() / 2]);
			p = mycontour[i];
			points.clear();
			points.push_back(mycontour[i]);
		}
		/*cout << mycontour[i] << endl;
		cv::circle(frame, mycontour[i], 2, cv::Scalar(200, 0, 255), 5);
		line(frame, mc, mycontour[i], Scalar(200, 0, 255), 1, 8);*/

	}
	for (int i = 0; i < fingers_points.size(); i++) {
		cv::circle(frame, fingers_points[i], 2, cv::Scalar(200, 0, 255), 5);
		line(frame, mc, fingers_points[i], Scalar(200, 0, 255), 1, 8); 
	}
}

int main()
{
	//extractContour();

	src = imread("HandOk.jpg", 1);

	namedWindow("Image", WINDOW_NORMAL);

	cvtColor(src, src_gray, COLOR_RGB2GRAY);

	namedWindow("Seuillage", WINDOW_NORMAL);

	threshold(src_gray, dst, threshold_value, max_BINARY_value, threshold_type);
	imwrite("HandOk_threshold.jpeg", dst);

	//Erosion puis dilatation = fermeture ==> permet d'affiner la forme de l'image seuillée
	Mat erode_element = getStructuringElement(MORPH_RECT, cv::Size(6, 6));
	Mat dilate_element = getStructuringElement(MORPH_RECT, cv::Size(6, 6));
	erode(dst, dst, erode_element);
	dilate(dst, dst, dilate_element);
	imwrite("HandOk_threshold_ouverture.jpeg", dst);

	vector<vector<cv::Point>> contours;
	vector<vector<cv::Point>> contours_int;
	vector<Vec4i> hierarchy;
	vector<Vec4i> hierarchy_int;
	findContours(dst, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	findContours(dst, contours_int, hierarchy_int, RETR_CCOMP, CHAIN_APPROX_SIMPLE);

	//hull correspond à l'enveloppe convexe de la forme
	vector<vector<cv::Point>> hull(contours.size());
	vector<vector<int>> inthull(contours.size());
	vector<vector<Vec4i>> defects(contours.size());

	for (int i = 0; i < contours_int.size(); i++) {
		drawContours(src, contours_int, i, CV_RGB(0, 0, 255), 2, 8, hierarchy_int);
		Point2f p = center_of_mass(contours_int[i]);
		cv::circle(src, p, 2, cv::Scalar(200, 0, 255), 5);
	}
	imwrite("HandOk_threshold_forms.jpeg", src);

	for (int i = 0; i < contours.size(); i++) {
		convexHull(Mat(contours[i]), hull[i], true);
		/*
		convexHull(Mat(contours[i]), inthull[i], true);
		if (inthull[i].size() > 3) // If number of hull > 3  we will go to find the convexity defect
			convexityDefects(contours[i], inthull[i], defects[i]);
		condefects(defects[i], contours[i], src);*/
		convex_point(hull[i], src);
		for (int j = 0; j < hull[i].size(); j++) {
			cout << "x : " << hull[i][j].x << " ";
			cout << "y : " << hull[i][j].y << endl;
			//cv::circle(src, hull[i][j], 2, cv::Scalar(200, 0, 255), 5);
		}
		//drawContours(src, hull, i, CV_RGB(0, 255, 0), 2, 8, hierarchy);
		//drawContours(src, contours, i, CV_RGB(0, 0, 255), 2, 8, hierarchy);
		std::cout << "Stats" << endl;
		std::cout << "Nombre de segments dans le contours : " << contours[i].size() << endl;
		std::cout << "Nombre de segments dans l'enveloppe : " << hull[i].size() << endl;
	}
	imwrite("HandOk_threshold_convex_hull_points.jpeg", src);

	imshow("Image", src);
	imshow("Seuillage", dst);
	waitKey(0);
    return 0;
}

